import React from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useRuntime } from "vtex.render-runtime"

interface SiteEditorIntegrationProps {
  text: string,
  buttonColor: string
  isH2: boolean
  link: string
  banner: string
  mobileBanner: string
  items: SiteEditorIntegrationItems[],
  ocultarTodo: boolean
}

interface SiteEditorIntegrationItems {
  text: string
  image: string
}

const CSS_HANDLES = ['buttonContainer', 'buttonText', 'buttonH2', 'customBanner', 'customBanner_img', 'gridImages', 'gridItem', 'gridItemImage', 'gridItemText']

const SiteEditorIntegration = ({ text, buttonColor, isH2, link, banner, mobileBanner, items, ocultarTodo }: SiteEditorIntegrationProps) => {
  const cssHandles = useCssHandles(CSS_HANDLES)
  const { deviceInfo } = useRuntime()
  console.log(items)
  return (
    <>
      {!ocultarTodo && (
        <>
          <a href={link ? link : ''} style={{ backgroundColor: buttonColor }} className={cssHandles.buttonContainer}>
            {isH2 ? (
              <h2 className={cssHandles.buttonH2}>
                {text}
              </h2>
            ) : (
              <span className={cssHandles.buttonText}>
                {text}
              </span>
            )}

          </a>
          {banner && (
            <div className={cssHandles.customBanner}>
              <img className={cssHandles.customBanner_img} src={deviceInfo.isMobile ? mobileBanner : banner} />
            </div>
          )}
          
          <div className={cssHandles.gridImages}>
            {items?.map((item: SiteEditorIntegrationItems, i: number) => {
              return (
                <div className={cssHandles.gridItem} key={i}>
                  <img className={cssHandles.gridItemImage} src={item.image}></img>
                  <p className={cssHandles.gridItemText}> {item.text} </p>
                </div>
              )
            })}
          </div>
        </>
      )}
    </>
  )
}

SiteEditorIntegration.schema = {
  title: "Site Editor Integration"
}

export default SiteEditorIntegration