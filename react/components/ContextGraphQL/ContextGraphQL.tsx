import React from 'react'

import { useLazyQuery } from 'react-apollo'
import { useProduct } from 'vtex.product-context';

// import GET_PRODUCT from '../../graphql/getProduct.graphql'
import GET_SHIPPING from '../../graphql/getShipping.graphql'

export const ContextGraphQL = () => {
  const productContext = useProduct()
  console.log(productContext)
  // const { data, loading, error } = useQuery(GET_PRODUCT, {
  //   variables: {
  //     idProduct: productContext?.product?.productId
  //   },
  //   ssr: false
  // });
  // console.log("Contexto producto", useProduct())
  // console.log("Data:", data, "Loading:", loading, "Error:", error);

  const [handleSearchShipping, {data, error, loading}] = useLazyQuery(GET_SHIPPING, {
    variables: {
      items: [
        {
          id: productContext?.selectedItem?.itemId,
          quantity: productContext?.selectedQuantity,
          seller: "agenciamagma"
        }
      ],
      postalCode: "28633-380",
      country: "BRA"
    },
    pollInterval: 500,
  })

  console.log(data, loading, error)
  
  return (
    <div>
      {productContext?.selectedItem?.sellers?.map((seller: any) => {
        return <div> Cantidad disponible para el seller {seller.sellerName}: {seller.commertialOffer?.AvailableQuantity} </div>
      })}
      
      <button onClick={handleSearchShipping as any}>
        Buscar Shipping Option
      </button>
    </div>
  )
}
