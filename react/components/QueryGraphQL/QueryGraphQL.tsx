import React from 'react'

import { useQuery } from 'react-apollo'

import GET_SESSION from '../../graphql/getSession.graphql'

export const QueryGraphQL = () => {
  const { data, loading, error } = useQuery(GET_SESSION, {
    ssr: false
  });
  
  console.log("Data:", data, "Loading:", loading, "Error:", error);

  return (
    <div>QueryGraphQL</div>
  )
}
